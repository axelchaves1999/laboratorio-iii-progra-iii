﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using PronosticoClima.ServiceReference1;


namespace PronosticoClima
{
    public partial class Form1 : Form
    {

        public WSMeteorologicoClient ws;

        public Form1()
        {
            ws = new WSMeteorologicoClient();
            InitializeComponent();
         
         
         
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CargarZonas();
            CargarEfemeridad();
        }

        private void CargarEfemeridad()
        {

            string efemeride = ws.efemerides(new efemerides());
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(efemeride);
            XmlNodeList lista = doc.GetElementsByTagName("EFEMERIDE_SOL");
            txtSolSale.Text = lista[0].ChildNodes[0].InnerText;
            txtSolSePone.Text = lista[0].ChildNodes[1].InnerText;
             lista = doc.GetElementsByTagName("EFEMERIDE_LUNA");
            txtLunaSale.Text = lista[0].ChildNodes[0].InnerText;
            txtLunaSePone.Text = lista[0].ChildNodes[1].InnerText;
            txtFaseLunar.Text = doc.GetElementsByTagName("FASELUNAR")[0].InnerText;


        }

        private void CargarCiudades(int id)
        {
            string ciudades = ws.pronosticoPorCiudad(new pronosticoCiudad());
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ciudades);
            XmlNodeList lista = doc.GetElementsByTagName("CIUDAD");
            cbCiudades.Items.Clear();
            for (int i = 0; i < lista.Count; i++)
            {
                if (Int32.Parse(lista[i].Attributes[0].InnerText).Equals(id))
                {
                    cbCiudades.Items.Add(lista[i].Attributes[1].InnerText);
                }
            }


        }

        private void CargarZonas()
        {

            string reg = ws.pronosticoRegional(new pronosticoRegion());

            foreach (string s in LeerZonas(reg))
            {
                cbZonas.Items.Add(s);
            }


        }

        private List<string> LeerCiudades(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlNodeList lista = doc.GetElementsByTagName("CIUDAD");
            List<String> txt = new List<string>();
            for (int i = 0; i < lista.Count; i++)
            {

                txt.Add(lista[i].Attributes[2].InnerText + "-" + lista[i].Attributes[1].InnerText);
            }
            return txt;

        }

        private List<string> LeerZonas(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlNodeList lista = doc.GetElementsByTagName("REGION");
            List<string> txt = new List<string>();
            for (int i = 0; i < lista.Count; i++)
            {
                txt.Add(lista[i].Attributes[1].InnerText);
            }
            return txt;

        }

        private void cbCiudades_TextChanged(object sender, EventArgs e)
        {
            int id = IdRegion();
            if (id != -1)
            {
                CargarCiudades(id);

            }

        }

        private int IdRegion()
        {
            string region = cbZonas.Text.Trim();
            string regiones = ws.pronosticoRegional(new pronosticoRegion());
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(regiones);
            XmlNodeList lista = doc.GetElementsByTagName("REGION");
            int id = 0;
            for (int i = 0; i < lista.Count; i++)
            {
                if (lista[i].Attributes[1].InnerText.Equals(region))
                {
                    id = Int32.Parse(lista[i].Attributes[0].InnerText);

                }

            }
            if (id != 0)
            {
                return id;

            }
            return -1;

        }

        private void CargarDatosCiudad(string xml)
        {

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlNodeList lista = doc.GetElementsByTagName("ESTADOMANANA");
            for (int i = 0; i < lista.Count; i++)
            {

                pbManana.ImageLocation = String.Format("https://www.imn.ac.cr{0}", lista[i].Attributes[0].InnerText);
                lbEstadoManana.Text = lista[i].InnerText;
                lista = doc.GetElementsByTagName("COMENTARIOMANANA");
                if (lista != null)
                {
                    txtComentarioManana.Text = lista[0].InnerText;

                }

            }
            lista = doc.GetElementsByTagName("ESTADOTARDE");
            for (int i = 0; i < lista.Count; i++)
            {
                pbTarde.ImageLocation = String.Format("https://www.imn.ac.cr{0}", lista[i].Attributes[0].InnerText);
                lbEstadoTarde.Text = lista[i].InnerText;
                lista = doc.GetElementsByTagName("COMENTARIOTARDE");
                if (lista != null)
                {
                    txtComentarioTarde.Text = lista[0].InnerText;
                }

            }
            lista = doc.GetElementsByTagName("ESTADONOCHE");
            for (int i = 0; i < lista.Count; i++)
            {
                pbNoche.ImageLocation = String.Format("https://www.imn.ac.cr{0}", lista[i].Attributes[0].InnerText);
                lbEstadoNoche.Text = lista[i].InnerText;
                lista = doc.GetElementsByTagName("COMENTARIONOCHE");
                if (lista != null)
                {
                    txtComentarioNoche.Text = lista[0].InnerText;
                }

            }
            lista = doc.GetElementsByTagName("TEMPMAX");
            txtTempMax.Text = lista[0].InnerText;
            lista = doc.GetElementsByTagName("TEMPMIN");
            txtTempMin.Text = lista[0].InnerText;

        }

        private void cbCiudades_TextChanged_1(object sender, EventArgs e)
        {
            string ciudad = cbCiudades.Text.Trim();
            string ciudades = ws.pronosticoPorCiudad(new pronosticoCiudad());
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(ciudades);
            XmlNodeList lista = doc.GetElementsByTagName("CIUDAD");
            for (int i = 0; i < lista.Count; i++)
            {
             
                if (lista[i].Attributes[1].InnerText.Equals(ciudad)) {

                    CargarDatosCiudad(lista[i].OuterXml);
                 
                }
            }

        }
    }
}
