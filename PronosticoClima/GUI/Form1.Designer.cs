﻿namespace PronosticoClima
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbZonas = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtFaseLunar = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLunaSePone = new System.Windows.Forms.TextBox();
            this.txtLunaSale = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSolSePone = new System.Windows.Forms.TextBox();
            this.txtSolSale = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtTempMin = new System.Windows.Forms.TextBox();
            this.txtTempMax = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbCiudades = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtComentarioNoche = new System.Windows.Forms.TextBox();
            this.lbEstadoNoche = new System.Windows.Forms.Label();
            this.pbNoche = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtComentarioTarde = new System.Windows.Forms.TextBox();
            this.lbEstadoTarde = new System.Windows.Forms.Label();
            this.pbTarde = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtComentarioManana = new System.Windows.Forms.TextBox();
            this.lbEstadoManana = new System.Windows.Forms.Label();
            this.pbManana = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbNoche)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbTarde)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbManana)).BeginInit();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbZonas
            // 
            this.cbZonas.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbZonas.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.cbZonas.FormattingEnabled = true;
            this.cbZonas.Location = new System.Drawing.Point(36, 34);
            this.cbZonas.Name = "cbZonas";
            this.cbZonas.Size = new System.Drawing.Size(251, 26);
            this.cbZonas.TabIndex = 0;
            this.cbZonas.TextChanged += new System.EventHandler(this.cbCiudades_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.cbCiudades);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.cbZonas);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(992, 573);
            this.panel1.TabIndex = 1;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txtFaseLunar);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.txtLunaSePone);
            this.panel7.Controls.Add(this.txtLunaSale);
            this.panel7.Location = new System.Drawing.Point(611, 334);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(369, 227);
            this.panel7.TabIndex = 6;
            // 
            // txtFaseLunar
            // 
            this.txtFaseLunar.Enabled = false;
            this.txtFaseLunar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFaseLunar.Location = new System.Drawing.Point(119, 165);
            this.txtFaseLunar.Name = "txtFaseLunar";
            this.txtFaseLunar.Size = new System.Drawing.Size(129, 26);
            this.txtFaseLunar.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(240, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 20);
            this.label9.TabIndex = 6;
            this.label9.Text = "Se pone";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(50, 64);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 20);
            this.label8.TabIndex = 5;
            this.label8.Text = "Sale";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(164, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 19);
            this.label5.TabIndex = 4;
            this.label5.Text = "Luna";
            // 
            // txtLunaSePone
            // 
            this.txtLunaSePone.Enabled = false;
            this.txtLunaSePone.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLunaSePone.Location = new System.Drawing.Point(209, 83);
            this.txtLunaSePone.Name = "txtLunaSePone";
            this.txtLunaSePone.Size = new System.Drawing.Size(129, 26);
            this.txtLunaSePone.TabIndex = 3;
            // 
            // txtLunaSale
            // 
            this.txtLunaSale.Enabled = false;
            this.txtLunaSale.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLunaSale.Location = new System.Drawing.Point(7, 87);
            this.txtLunaSale.Name = "txtLunaSale";
            this.txtLunaSale.Size = new System.Drawing.Size(129, 26);
            this.txtLunaSale.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel9);
            this.panel6.Controls.Add(this.txtSolSePone);
            this.panel6.Controls.Add(this.txtSolSale);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Location = new System.Drawing.Point(611, 184);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(369, 134);
            this.panel6.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(159, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 19);
            this.label7.TabIndex = 4;
            this.label7.Text = "Sol";
            // 
            // txtSolSePone
            // 
            this.txtSolSePone.Enabled = false;
            this.txtSolSePone.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSolSePone.Location = new System.Drawing.Point(209, 87);
            this.txtSolSePone.Name = "txtSolSePone";
            this.txtSolSePone.Size = new System.Drawing.Size(129, 26);
            this.txtSolSePone.TabIndex = 3;
            // 
            // txtSolSale
            // 
            this.txtSolSale.Enabled = false;
            this.txtSolSale.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSolSale.Location = new System.Drawing.Point(7, 87);
            this.txtSolSale.Name = "txtSolSale";
            this.txtSolSale.Size = new System.Drawing.Size(129, 26);
            this.txtSolSale.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(240, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 20);
            this.label3.TabIndex = 1;
            this.label3.Text = "Se pone";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Sale";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel8);
            this.panel5.Controls.Add(this.txtTempMin);
            this.panel5.Controls.Add(this.txtTempMax);
            this.panel5.Location = new System.Drawing.Point(611, 34);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(369, 134);
            this.panel5.TabIndex = 4;
            // 
            // txtTempMin
            // 
            this.txtTempMin.Enabled = false;
            this.txtTempMin.Location = new System.Drawing.Point(209, 84);
            this.txtTempMin.Name = "txtTempMin";
            this.txtTempMin.Size = new System.Drawing.Size(129, 26);
            this.txtTempMin.TabIndex = 3;
            // 
            // txtTempMax
            // 
            this.txtTempMax.Enabled = false;
            this.txtTempMax.Location = new System.Drawing.Point(7, 84);
            this.txtTempMax.Name = "txtTempMax";
            this.txtTempMax.Size = new System.Drawing.Size(129, 26);
            this.txtTempMax.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(193, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Temperatura minima";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(3, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Temperatura maxima";
            // 
            // cbCiudades
            // 
            this.cbCiudades.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbCiudades.ForeColor = System.Drawing.SystemColors.Highlight;
            this.cbCiudades.FormattingEnabled = true;
            this.cbCiudades.Location = new System.Drawing.Point(293, 34);
            this.cbCiudades.Name = "cbCiudades";
            this.cbCiudades.Size = new System.Drawing.Size(250, 27);
            this.cbCiudades.TabIndex = 3;
            this.cbCiudades.TextChanged += new System.EventHandler(this.cbCiudades_TextChanged_1);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel4.Controls.Add(this.txtComentarioNoche);
            this.panel4.Controls.Add(this.lbEstadoNoche);
            this.panel4.Controls.Add(this.pbNoche);
            this.panel4.Location = new System.Drawing.Point(36, 414);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(507, 100);
            this.panel4.TabIndex = 2;
            // 
            // txtComentarioNoche
            // 
            this.txtComentarioNoche.BackColor = System.Drawing.SystemColors.MenuBar;
            this.txtComentarioNoche.Enabled = false;
            this.txtComentarioNoche.Location = new System.Drawing.Point(131, 32);
            this.txtComentarioNoche.Multiline = true;
            this.txtComentarioNoche.Name = "txtComentarioNoche";
            this.txtComentarioNoche.Size = new System.Drawing.Size(244, 60);
            this.txtComentarioNoche.TabIndex = 4;
            // 
            // lbEstadoNoche
            // 
            this.lbEstadoNoche.AutoSize = true;
            this.lbEstadoNoche.Location = new System.Drawing.Point(131, 9);
            this.lbEstadoNoche.Name = "lbEstadoNoche";
            this.lbEstadoNoche.Size = new System.Drawing.Size(51, 20);
            this.lbEstadoNoche.TabIndex = 3;
            this.lbEstadoNoche.Text = "Estado";
            // 
            // pbNoche
            // 
            this.pbNoche.Location = new System.Drawing.Point(0, 0);
            this.pbNoche.Name = "pbNoche";
            this.pbNoche.Size = new System.Drawing.Size(121, 100);
            this.pbNoche.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbNoche.TabIndex = 1;
            this.pbNoche.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Location = new System.Drawing.Point(36, 99);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(507, 462);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ciudad";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel3.Controls.Add(this.txtComentarioTarde);
            this.panel3.Controls.Add(this.lbEstadoTarde);
            this.panel3.Controls.Add(this.pbTarde);
            this.panel3.Location = new System.Drawing.Point(0, 181);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(507, 100);
            this.panel3.TabIndex = 1;
            // 
            // txtComentarioTarde
            // 
            this.txtComentarioTarde.BackColor = System.Drawing.SystemColors.MenuBar;
            this.txtComentarioTarde.Enabled = false;
            this.txtComentarioTarde.Location = new System.Drawing.Point(131, 32);
            this.txtComentarioTarde.Multiline = true;
            this.txtComentarioTarde.Name = "txtComentarioTarde";
            this.txtComentarioTarde.Size = new System.Drawing.Size(244, 60);
            this.txtComentarioTarde.TabIndex = 4;
            // 
            // lbEstadoTarde
            // 
            this.lbEstadoTarde.AutoSize = true;
            this.lbEstadoTarde.Location = new System.Drawing.Point(131, 9);
            this.lbEstadoTarde.Name = "lbEstadoTarde";
            this.lbEstadoTarde.Size = new System.Drawing.Size(51, 20);
            this.lbEstadoTarde.TabIndex = 3;
            this.lbEstadoTarde.Text = "Estado";
            // 
            // pbTarde
            // 
            this.pbTarde.Location = new System.Drawing.Point(0, 0);
            this.pbTarde.Name = "pbTarde";
            this.pbTarde.Size = new System.Drawing.Size(121, 100);
            this.pbTarde.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbTarde.TabIndex = 1;
            this.pbTarde.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.txtComentarioManana);
            this.panel2.Controls.Add(this.lbEstadoManana);
            this.panel2.Controls.Add(this.pbManana);
            this.panel2.Location = new System.Drawing.Point(0, 37);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(507, 100);
            this.panel2.TabIndex = 0;
            // 
            // txtComentarioManana
            // 
            this.txtComentarioManana.BackColor = System.Drawing.SystemColors.MenuBar;
            this.txtComentarioManana.Enabled = false;
            this.txtComentarioManana.Location = new System.Drawing.Point(127, 37);
            this.txtComentarioManana.Multiline = true;
            this.txtComentarioManana.Name = "txtComentarioManana";
            this.txtComentarioManana.Size = new System.Drawing.Size(244, 60);
            this.txtComentarioManana.TabIndex = 2;
            // 
            // lbEstadoManana
            // 
            this.lbEstadoManana.AutoSize = true;
            this.lbEstadoManana.Location = new System.Drawing.Point(127, 14);
            this.lbEstadoManana.Name = "lbEstadoManana";
            this.lbEstadoManana.Size = new System.Drawing.Size(51, 20);
            this.lbEstadoManana.TabIndex = 1;
            this.lbEstadoManana.Text = "Estado";
            // 
            // pbManana
            // 
            this.pbManana.Location = new System.Drawing.Point(0, 0);
            this.pbManana.Name = "pbManana";
            this.pbManana.Size = new System.Drawing.Size(121, 100);
            this.pbManana.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbManana.TabIndex = 0;
            this.pbManana.TabStop = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel8.Controls.Add(this.label1);
            this.panel8.Controls.Add(this.label2);
            this.panel8.Location = new System.Drawing.Point(0, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(369, 62);
            this.panel8.TabIndex = 4;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel9.Controls.Add(this.label7);
            this.panel9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel9.Location = new System.Drawing.Point(2, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(369, 49);
            this.panel9.TabIndex = 5;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.HotTrack;
            this.panel10.Controls.Add(this.label5);
            this.panel10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel10.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel10.Location = new System.Drawing.Point(608, 334);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(372, 49);
            this.panel10.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 573);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estado del clima";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbNoche)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbTarde)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbManana)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbZonas;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pbNoche;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pbTarde;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pbManana;
        private System.Windows.Forms.TextBox txtComentarioManana;
        private System.Windows.Forms.Label lbEstadoManana;
        private System.Windows.Forms.TextBox txtComentarioNoche;
        private System.Windows.Forms.Label lbEstadoNoche;
        private System.Windows.Forms.TextBox txtComentarioTarde;
        private System.Windows.Forms.Label lbEstadoTarde;
        private System.Windows.Forms.ComboBox cbCiudades;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtTempMin;
        private System.Windows.Forms.TextBox txtTempMax;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtFaseLunar;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLunaSePone;
        private System.Windows.Forms.TextBox txtLunaSale;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSolSePone;
        private System.Windows.Forms.TextBox txtSolSale;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel8;
    }
}

