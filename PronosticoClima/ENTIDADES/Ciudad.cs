﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PronosticoClima.ENTIDADES
{
    class Ciudad
    {
 
        public string TemperaturaMax { get; set;}
        public string TemperaturaMin { get; set; }
        public string EstadoManana { get; set; }
        public string EstadoTarde { get; set; }
        public string EstadoNoche { get; set; }
        public string ComentarioManana { get; set; }
        public string ComentarioTarde { get; set; }
        public string ComentarioNoche { get; set; }

    }
}
